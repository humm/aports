# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=pimcommon
pkgver=23.08.1
pkgrel=1
pkgdesc="Common lib for KDEPim"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi
arch="all !armhf !ppc64le !s390x !riscv64"
url='https://community.kde.org/KDE_PIM'
license="GPL-2.0-or-later"
depends_dev="
	akonadi-contacts-dev
	akonadi-dev
	karchive5-dev
	kcodecs5-dev
	kcompletion5-dev
	kconfig5-dev
	kconfigwidgets5-dev
	kcontacts5-dev
	kcoreaddons5-dev
	kdbusaddons5-dev
	ki18n5-dev
	kiconthemes5-dev
	kimap-dev
	kio5-dev
	kitemmodels5-dev
	kjobwidgets5-dev
	kmime-dev
	knewstuff5-dev
	kpimtextedit-dev
	kservice5-dev
	ktextaddons-dev
	kwidgetsaddons5-dev
	kxmlgui5-dev
	libkdepim-dev
	purpose5-dev
	qt5-qtbase-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/pimcommon.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/pimcommon-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"
options="net" # net required for tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_DESIGNERPLUGIN=ON
	cmake --build build
}

check() {
	xvfb-run ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
76d535282fc945e55e020efee0414f0f5bb5558d8b13e358edc3e6920cb8021f397fcf8a01932d3a3de5d229a803efd901fa4786df0d7af44e7cca0a00466b0b  pimcommon-23.08.1.tar.xz
"
