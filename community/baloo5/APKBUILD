# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-frameworks
pkgname=baloo5
pkgver=5.110.0
pkgrel=1
pkgdesc="A framework for searching and managing metadata"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://community.kde.org/Frameworks"
license="LGPL-2.1-or-later AND ( LGPL-2.1-only OR LGPL-3.0-only )"
depends_dev="
	kbookmarks5-dev
	kcompletion5-dev
	kconfig5-dev
	kcoreaddons5-dev
	kcrash5-dev
	kdbusaddons5-dev
	kfilemetadata5-dev
	ki18n5-dev
	kidletime5-dev
	kio5-dev
	kjobwidgets5-dev
	kservice5-dev
	lmdb-dev
	qt5-qtdeclarative-dev
	solid5-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/frameworks/baloo.git"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/baloo-$pkgver.tar.xz"
subpackages="$pkgname-dbg $pkgname-dev $pkgname-lang"
options="!check" # Tons of broken tests
builddir="$srcdir/baloo-$pkgver"

replaces="baloo<=5.110.0-r0"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build

	# We don't ship systemd
	rm -r "$pkgdir"/usr/lib/systemd
}

sha512sums="
f5bfd313bb72f57899c3be845dfc699cfcc1c9286733fe0ff5d09cb654b793282dfda8a59a185107686105b7eec2f11b623dbbf1059dd7cb993ceb6295bcc673  baloo-5.110.0.tar.xz
"
