# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=systemsettings
pkgver=5.27.8
pkgrel=1
pkgdesc="Plasma system manager for hardware, software, and workspaces"
# armhf blocked by qt5-qtdeclarative
arch="all !armhf"
url="https://kde.org/plasma-desktop/"
license="GPL-2.0-or-later"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	kactivities5-dev
	kactivities-stats5-dev
	kcmutils5-dev
	kconfig5-dev
	kcrash5-dev
	kdbusaddons5-dev
	kdeclarative5-dev
	kdoctools5-dev
	khtml-dev
	ki18n5-dev
	kiconthemes5-dev
	kio5-dev
	kirigami2-dev
	kitemviews5-dev
	kpackage5-dev
	kservice5-dev
	kwidgetsaddons5-dev
	kwindowsystem5-dev
	kxmlgui5-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	samurai
	"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/systemsettings.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/systemsettings-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-zsh-completion $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
6515e2b22360a857b48057ee19ff05104426119b3634316c32f85f6434a063843994b5f772ff75aa60dc58ba011dc8681399c0c646e1e099b37c5332c664ffae  systemsettings-5.27.8.tar.xz
"
