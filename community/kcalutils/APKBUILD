# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kcalutils
pkgver=23.08.1
pkgrel=1
pkgdesc="The KDE calendar utility library"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://api.kde.org/kdepim/kcalutils/html"
license="LGPL-2.0-or-later"
depends_dev="
	grantlee-dev
	kcalendarcore5-dev
	kcodecs5-dev
	kconfig5-dev
	kconfigwidgets5-dev
	kcoreaddons5-dev
	ki18n5-dev
	kiconthemes5-dev
	kidentitymanagement-dev
	kwidgetsaddons5-dev
	"
makedepends="
	$depends_dev
	extra-cmake-modules
	samurai
	"
checkdepends="xvfb-run"
_repo_url="https://invent.kde.org/pim/kcalutils.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kcalutils-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build

	# kcalutils-testincidenceformatter and kcalutils-testtodotooltip are broken
	CTEST_OUTPUT_ON_FAILURE=TRUE xvfb-run ctest -E "kcalutils-test(incidenceformatter|todotooltip|dndfactory)"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2e61dc2f437438550822d5aedff974407a98bb30dcf5c7238bc0de7ff9dee0e99e2aea3bbdf2e3f919ffef30d5c0a4d49a1de0a9c477356cf0af6d746356803c  kcalutils-23.08.1.tar.xz
"
