# Contributor: Francesco Colista <fcolista@alpinelinux.org>
# Maintainer: Francesco Colista <fcolista@alpinelinux.org>
pkgname=certbot-nginx
pkgver=2.7.0
pkgrel=0
pkgdesc="Nginx plugin for Certbot client"
url="https://github.com/certbot/certbot"
arch="noarch"
license="Apache-2.0"
depends="
	certbot
	py3-acme
	py3-openssl
	py3-parsing
	py3-setuptools
	"
makedepends="
	py3-gpep517
	py3-wheel
	"
checkdepends="py3-pytest-xdist"
subpackages="$pkgname-pyc"
source="$pkgname-$pkgver.tar.gz::https://github.com/certbot/certbot/archive/refs/tags/v$pkgver.tar.gz"
builddir="$srcdir/certbot-$pkgver/$pkgname"

build() {
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest -n auto -p no:warnings
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
1e1defcd143f804a36b50e3104d239bf20873545474ceb654269a5879e15cfbb76a18bd569f5e6e12de1036779e72c74d9896707daabc314fc5d67acc1c5180d  certbot-nginx-2.7.0.tar.gz
"
