# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-plasma
pkgname=kde-cli-tools
pkgver=5.27.8
pkgrel=1
pkgdesc="Tools based on KDE Frameworks 5 to better interact with the system"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://invent.kde.org/plasma/kde-cli-tools"
license="(GPL-2.0-only OR GPL-3.0-only) AND GPL-2.0-or-later AND GPL-2.0-only AND LGPL-2.1-only"
makedepends="
	extra-cmake-modules
	kactivities5-dev
	kcmutils5-dev
	kconfig5-dev
	kdeclarative5-dev
	kdesu5-dev
	kdoctools5-dev
	ki18n5-dev
	kiconthemes5-dev
	kinit5-dev
	kio5-dev
	kservice5-dev
	kwindowsystem5-dev
	plasma-workspace-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	qt5-qtx11extras-dev
	samurai
	"
checkdepends="xvfb-run"

case "$pkgver" in
	*.90*) _rel=unstable;;
	*) _rel=stable;;
esac
_repo_url="https://invent.kde.org/plasma/kde-cli-tools.git"
source="https://download.kde.org/$_rel/plasma/$pkgver/kde-cli-tools-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # Broken

# Workaround a circular dependency https://gitlab.alpinelinux.org/alpine/aports/-/issues/11785
install_if="plasma-workspace"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
679756c8e4ac0602e2a8237c4947e11777cd51ef97b765e7d03233d2e71f917abff1ea219a0d5063a1908cb9a99718857232fe61f7e1ad7e14ccc7c55410d290  kde-cli-tools-5.27.8.tar.xz
"
