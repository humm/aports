# Contributor: Bart Ribbers <bribbers@disroot.org>
# Contributor: Danct12 <danct12@disroot.org>
# Contributor: Clayton Craft <clayton@craftyguy.net>
# Contributor: Rasmus Thomsen <oss@cogitri.dev>
# Maintainer: Newbyte <newbie13xd@gmail.com>
pkgname=phoc
pkgver=0.32.0
pkgrel=0
pkgdesc="wlroots based Phone compositor for the Phosh shell"
arch="all !s390x" # blocked by gnome-desktop
url="https://gitlab.gnome.org/World/Phosh/phoc"
license="GPL-3.0-only"
depends="
	dbus
	mutter-schemas
	gsettings-desktop-schemas
	"
_wlrootsmakedepends="
	eudev-dev
	hwdata-dev
	libcap-dev
	libseat-dev
	libxcb-dev
	xcb-util-image-dev
	xcb-util-renderutil-dev
	xcb-util-wm-dev
	xkeyboard-config-dev
	xwayland-dev
	"
makedepends="
	glib-dev
	gnome-desktop-dev
	json-glib-dev
	libdrm-dev
	libinput-dev
	libxkbcommon-dev
	mesa-dev
	meson
	pixman-dev
	wayland-dev
	wayland-protocols
	$_wlrootsmakedepends
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dbg"
options="!check" # Needs fullblown EGL
source="
	https://storage.puri.sm/releases/phoc/phoc-$pkgver.tar.xz
	"
replaces="wlroots-phosh"

prepare() {
	default_prepare

	patch -Np1 < subprojects/packagefiles/wlroots/0001-Revert-layer-shell-error-on-0-dimension-without-anch.patch -d subprojects/wlroots
}

build() {
	abuild-meson \
		-Db_lto=true \
		-Dembed-wlroots=enabled \
		-Dtests="$(want_check && echo true || echo false)" \
		--default-library=static \
		. output
	meson compile -C output
}

check() {
	xvfb-run -a meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir/" meson install --no-rebuild -C output
	install -Dm755 helpers/scale-to-fit \
		-t "$pkgdir"/usr/bin

	# remove unneeded wlroots dev. files
	rm -r "$pkgdir"/usr/include
	rm -r "$pkgdir"/usr/lib/libwlroots.a
	rm -r "$pkgdir"/usr/lib/pkgconfig
}

sha512sums="
aa053c096e0e1e5f52b9a5230671604ce0af3871725f0e7e15d41b9d9669c20d0634cdc4562697b6c3385ff6a3c7769be44f7f51cbe20bec2e387b89060a1f9e  phoc-0.32.0.tar.xz
"
