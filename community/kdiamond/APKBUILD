# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdiamond
pkgver=23.08.1
pkgrel=1
pkgdesc="A single player puzzle game"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/games/kdiamond/"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kconfig5-dev
	kconfigwidgets5-dev
	kcoreaddons5-dev
	kcrash5-dev
	kdbusaddons5-dev
	kdoctools5-dev
	ki18n5-dev
	knotifications5-dev
	knotifyconfig5-dev
	kwidgetsaddons5-dev
	kxmlgui5-dev
	libkdegames-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/kdiamond.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdiamond-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
2b63ee94fb8211d9d9714ca3cd6fe28118d556858c14147417b0d69c10e1e7407469e16df9533139c4f826aab14c67186ac85f07b23dbf5329bf811bccfccaf5  kdiamond-23.08.1.tar.xz
"
