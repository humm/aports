# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kgpg
pkgver=23.08.1
pkgrel=1
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt5-qtwebengine -> akonadi-contacts
arch="all !armhf !ppc64le !s390x !riscv64"
url="https://kde.org/applications/utilities/org.kde.kgpg"
pkgdesc="A simple interface for GnuPG, a powerful encryption utility"
license="GPL-2.0-or-later"
makedepends="
	akonadi-contacts-dev
	extra-cmake-modules
	gpgme-dev
	karchive5-dev
	kcodecs5-dev
	kcontacts5-dev
	kcoreaddons5-dev
	kcrash5-dev
	kdbusaddons5-dev
	kdoctools5-dev
	ki18n5-dev
	kiconthemes5-dev
	kio5-dev
	kjobwidgets5-dev
	knotifications5-dev
	kservice5-dev
	ktextwidgets5-dev
	kwidgetsaddons5-dev
	kwindowsystem5-dev
	kxmlgui5-dev
	qt5-qtbase-dev
	samurai
	"
_repo_url="https://invent.kde.org/utilities/kgpg.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kgpg-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	# kgpg-import fails too often
	# kgpg-encrypt and kgpg-export are broken
	# del-key fails randomly
	# genkey fails on arm on builders
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest -E "kgpg-(genkey|import|encrypt|export|del-key)" -j1
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a7926a19b9fcb085e0affe7736effdfe2803e35d1f1c4a96df48a1cca72812d3bd736ab7164d90ddd378d798026d504d047fa954849cfe2f89c4b45f5aea407c  kgpg-23.08.1.tar.xz
"
