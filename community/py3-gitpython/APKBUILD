# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=py3-gitpython
pkgver=3.1.37
pkgrel=0
pkgdesc="Python3 Git Library"
url="https://github.com/gitpython-developers/GitPython"
arch="noarch"
license="BSD-3-Clause"
depends="
	git
	py3-gitdb2
	py3-typing-extensions
	python3
	"
makedepends="py3-setuptools"
checkdepends="
	py3-pytest
	py3-pytest-cov
	py3-pytest-sugar
	py3-toml
	"
subpackages="$pkgname-pyc"
source="https://github.com/gitpython-developers/GitPython/archive/$pkgver/GitPython-$pkgver.tar.gz"
builddir="$srcdir/GitPython-$pkgver"

# secfixes:
#   3.1.37-r0:
#     - CVE-2023-41040

build() {
	python3 setup.py build
}

check() {
	# There are more tests but they require a specific git configuration
	pytest test/test_config.py
}

package() {
	python3 setup.py install --skip-build --root="$pkgdir"
}

sha512sums="
6eab5aaf3a12220bd0d72afd4aa02558f2c33a8aa042ddc62f4cb0136bb08db0a57df8af4756e03f6faa239403989e3abcbc320fb982c71d57b2f3db11b7b546  GitPython-3.1.37.tar.gz
"
