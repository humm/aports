# Contributor: Peter Bui <pnutzh4x0r@gmail.com>
# Contributor: Sören Tempel <soeren+alpine@soeren-tempel.net>
# Contributor: Timo Teräs <timo.teras@iki.fi>
# Contributor: Leo <thinkabit.ukim@gmail.com>
# Contributor: Sodface <sod@sodface.com>
# Maintainer: lauren n. liberda <lauren@selfisekai.rocks>
pkgname=yt-dlp
pkgver=2023.10.07
pkgrel=0
pkgdesc="Command-line program to download videos from YouTube"
url="https://github.com/yt-dlp/yt-dlp"
arch="noarch"
license="Unlicense"
depends="python3"
_extradeps="
	attr
	ca-certificates
	ffmpeg
	py3-brotli
	py3-mutagen
	py3-pycryptodomex
	py3-secretstorage
	py3-websockets
	"
makedepends="$_extradeps py3-setuptools"
checkdepends="py3-flake8 py3-nose py3-pytest-xdist"
subpackages="
	$pkgname-core-pyc
	$pkgname-core
	$pkgname-doc
	$pkgname-zsh-completion
	$pkgname-bash-completion
	$pkgname-fish-completion
	"
source="$pkgname-$pkgver.tar.gz::https://github.com/yt-dlp/yt-dlp/releases/download/$pkgver/yt-dlp.tar.gz
	"
builddir="$srcdir/$pkgname"

# secfixes:
#   2023.09.24-r0:
#     - CVE-2023-40581
#   2023.07.06-r0:
#     - CVE-2023-35934

prepare() {
	default_prepare

	:> requirements.txt
}

build() {
	python3 setup.py build

	make completions
}

check() {
	PYTHON=/usr/bin/python3 make offlinetest
}

package() {
	depends="
		$_extradeps
		yt-dlp-core=$pkgver-r$pkgrel
		"

	python3 setup.py install --skip-build --root="$pkgdir"
	ln -sfv yt-dlp "$pkgdir"/usr/bin/youtube-dl
}

core() {
	# provide compat for removed old youtube-dl
	provides="youtube-dl=$pkgver-r$pkgrel"
	replaces="youtube-dl"

	amove usr/lib/python3*/site-packages/
	amove usr/bin
}

sha512sums="
21dcb85807b87446faadbdd20206cf3a9da67a575bbb8bdbf40f7af34e7b1d2530c9e15f76a28d3021c91b68bb6762d46a66446ca43c83ac02880b710286435d  yt-dlp-2023.10.07.tar.gz
"
