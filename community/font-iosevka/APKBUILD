# Maintainer: Will Sinatra <wpsinatra@gmail.com>
pkgname=font-iosevka
pkgver=27.2.0
pkgrel=0
pkgdesc="Versatile typeface for code, from code"
url="https://typeof.net/Iosevka/"
arch="noarch"
options="!check" # no testsuite
license="OFL-1.1"
depends="fontconfig"
subpackages="
	$pkgname-base
	$pkgname-aile
	$pkgname-slab
	$pkgname-curly
	$pkgname-curly-slab:curly_slab
	"
source="
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-aile-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-slab-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-$pkgver.zip
	https://github.com/be5invis/Iosevka/releases/download/v$pkgver/super-ttc-iosevka-curly-slab-$pkgver.zip
	"

builddir="$srcdir"

package() {
	depends="
		$pkgname-base=$pkgver-r$pkgrel
		$pkgname-aile=$pkgver-r$pkgrel
		$pkgname-slab=$pkgver-r$pkgrel
		$pkgname-curly=$pkgver-r$pkgrel
		$pkgname-curly-slab=$pkgver-r$pkgrel
		"

	install -Dm644 "$builddir"/*.ttc \
		-t "$pkgdir"/usr/share/fonts/${pkgname#font-}
}

base() {
	pkgdesc="$pkgdesc (Iosevka)"
	amove usr/share/fonts/iosevka/iosevka.ttc
}

aile() {
	pkgdesc="$pkgdesc (Iosevka Aile)"
	amove usr/share/fonts/iosevka/iosevka-aile.ttc
}

slab() {
	pkgdesc="$pkgdesc (Iosevka Slab)"
	amove usr/share/fonts/iosevka/iosevka-slab.ttc
}

curly() {
	pkgdesc="$pkgdesc (Iosevka Curly)"
	amove usr/share/fonts/iosevka/iosevka-curly.ttc
}

curly_slab() {
	pkgdesc="$pkgdesc (Iosevka Curly Slab)"
	amove usr/share/fonts/iosevka/iosevka-curly-slab.ttc
}

sha512sums="
7704442b8936c83a9f574ff15bb127bed5dfa64999cfd67d3f0646a48c938d6b65e9e622d469a3ee5fec818af7c34a9d989cc57421b297ca6f3432c35c4fb467  super-ttc-iosevka-27.2.0.zip
90378dc155d58233cd695a606869f1397d689d763acabf608d2c1d6b7feaf382191adbe5a776abdf71d530697a084ce7d62ecd4d9da5c92196cd5c096fb27d3f  super-ttc-iosevka-aile-27.2.0.zip
ae4ce6adbab9b46ba977ebd86c825631d2ebe67b81271a93abdf8c18c82a31e0e5b19cc918f9effad5e3d757a03c3408887f8ae2bff9506194939c9167574dba  super-ttc-iosevka-slab-27.2.0.zip
5cefa781478b61d48bb8a3578eb8e8d0db50e40596e17cdf256368152656ce033b1d67a798ec9db17a0632ccfb9505e598d2fa1587b83965257b56ea47305900  super-ttc-iosevka-curly-27.2.0.zip
d6273fd171c0ffe66d1eb02b0071a7e1f2fe3c36f550ef2795afed39d263cf3a57d59aea78682f0a844a180feb216fca81955fba662db05bbc9d01789db9c74a  super-ttc-iosevka-curly-slab-27.2.0.zip
"
