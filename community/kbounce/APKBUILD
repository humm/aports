# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kbounce
pkgver=23.08.1
pkgrel=1
pkgdesc="A single player arcade game with the elements of puzzle"
url="https://kde.org/applications/games/kbounce/"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
license="GPL-2.0-or-later AND LGPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcompletion5-dev
	kconfig5-dev
	kconfigwidgets5-dev
	kcoreaddons5-dev
	kcrash5-dev
	kdbusaddons5-dev
	kdoctools5-dev
	ki18n5-dev
	kio5-dev
	kwidgetsaddons5-dev
	kxmlgui5-dev
	libkdegames-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	samurai
	"
_repo_url="https://invent.kde.org/games/kbounce.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kbounce-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
e6c1ba07b0cc0a8b8ab21d7c2a6332fe46995f48e5cbebb2452e0c72f3d9ffea40e40df58ca3ee9610083c19273e32a585e79f0c4b4d15591c0c968e75e71e52  kbounce-23.08.1.tar.xz
"
