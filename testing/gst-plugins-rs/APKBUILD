# Maintainer: Krassy Boykinov <kboykinov@teamcentrixx.com>
pkgname=gst-plugins-rs
pkgver=0.11.0
pkgrel=0
pkgdesc="Gstreamer rust plugins"
url="https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs"
# ppc64le, s390x, riscv64: ring in gst-plugin-version-helper
# armv7, armhf, x86: ring in gtk4
arch="all !armhf !armv7 !ppc64le !riscv64 !s390x !x86"
license="MIT AND Apache-2.0 AND MPL-2.0 AND LGPL-2.1-or-later"
makedepends="
	cargo
	cargo-c
	dav1d-dev
	gst-plugins-bad-dev
	gst-plugins-base-dev
	gtk4.0-dev
	libsodium-dev
	meson
	nasm
	openssl-dev
	"
subpackages="$pkgname-dev $pkgname-tools"
source="
	https://gitlab.freedesktop.org/gstreamer/gst-plugins-rs/-/archive/$pkgver/gst-plugins-rs-$pkgver.tar.gz
	Cargo.lock.patch
	dylib.patch
	"
options="net !check" # they don't run

export SODIUM_USE_PKG_CONFIG=1

prepare() {
	default_prepare

	cargo fetch --target="$CTARGET" --locked
}

build() {
	export CARGO_PROFILE_RELEASE_OPT_LEVEL=3
	abuild-meson \
		--buildtype=release \
		. output
	meson compile -C output
}

check() {
	meson test --no-rebuild --print-errorlogs -C output
}

package() {
	DESTDIR="$pkgdir" meson install --no-rebuild -C output
}

tools() {
	amove usr/bin
}

sha512sums="
1d8ecd7812a96ead4cada31cfdfdf4eb125c704b12726e6a1533ac70dedaa333d1786dac0199271608a6df1ed238f9d1cd3f5d98c3fd34a10cc3fa54a6ed7cad  gst-plugins-rs-0.11.0.tar.gz
cb67c2a2c4dc121df67c4798e118748ac090b68458d5fbad6aab95d21742bcf783b74a3af93c65dc28d18a3f3962eed167e82bb9a5cc670d5c512c32a2213dd0  Cargo.lock.patch
5f354a7776859f62a235947b5a31779688bc681f3c47c3fbf85806c8a12f68023a731067c958e40dfd580af591ea271e4e5184ef3b45a193c9b855486c64fef0  dylib.patch
"
