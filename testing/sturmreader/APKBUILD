# Contributor: Emanuele Sorce <emanuele.sorce@hotmail.com>
# Maintainer: Emanuele Sorce <emanuele.sorce@hotmail.com>
pkgname=sturmreader
pkgver=3.7.1
pkgrel=0
pkgdesc="E-Book and comic reader for mobile devices"
url="https://gitlab.com/tronfortytwo/sturmreader"
arch="aarch64 armv7 x86 x86_64" # Limited by qt5-qtwebengine
license="GPL-3.0-or-later"
depends="
	qt5-qtbase-sqlite
	qt5-qtquickcontrols
	qt5-qtsvg
	qt5-qtsystems
	"
makedepends="
	cmake
	gettext-dev
	poppler-qt5-dev
	qt5-qtdeclarative-dev
	qt5-qtlocation-dev
	qt5-qtquickcontrols2-dev
	qt5-qtsvg-dev
	qt5-qtwebchannel-dev
	qt5-qtwebengine-dev
	quazip-dev
	samurai
	"
subpackages="$pkgname-lang"
source="https://gitlab.com/tronfortytwo/sturmreader/-/archive/$pkgver/sturmreader-$pkgver.tar.gz
	intl.patch
	"

# Sturm reader has no testing! Not even a --version thing
options="!check"

build() {
	CXXFLAGS="$CXXFLAGS -flto=auto" \
	cmake -G Ninja -B build -Wno-dev \
		-DCLICK_MODE=OFF \
		-DCMAKE_BUILD_TYPE=MinSizeRel
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
cb3246e68735c5a13b80884307da8d631116a9e709698ef339300f3f2266104abfd4ba1a0d21424ca0908778c9d17eb1fe51ec8e3a4fae12f77160130672e091  sturmreader-3.7.1.tar.gz
287f48924bc3fb60c651e54d687872baf49882aa009c28a27de5fca60c586a7a6daf00c8dbfdfb45efaec6688253966a4c5de2973e7d0a75ceac197cfe879c60  intl.patch
"
