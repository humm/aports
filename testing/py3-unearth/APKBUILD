# Maintainer: Patrycja Rosa <alpine@ptrcnull.me>
pkgname=py3-unearth
pkgver=0.11.1
pkgrel=0
pkgdesc="Utility to fetch and download python packages"
url="https://github.com/frostming/unearth"
arch="noarch"
license="MIT"
depends="py3-packaging py3-requests"
makedepends="py3-pdm-backend py3-gpep517 py3-installer py3-trustme"
checkdepends="py3-pytest py3-flask py3-requests-wsgi-adapter py3-pytest-httpserver"
subpackages="$pkgname-pyc"
source="https://github.com/frostming/unearth/archive/$pkgver/py3-unearth-$pkgver.tar.gz"
builddir="$srcdir/unearth-$pkgver"

build() {
	export PDM_BUILD_SCM_VERSION=$pkgver
	gpep517 build-wheel \
		--wheel-dir .dist \
		--output-fd 3 3>&1 >&2
}

check() {
	python3 -m venv --clear --without-pip --system-site-packages .testenv
	.testenv/bin/python3 -m installer .dist/*.whl
	.testenv/bin/python3 -m pytest
}

package() {
	python3 -m installer -d "$pkgdir" \
		.dist/*.whl
}

sha512sums="
fc01d0211b213ab6e5d68f34bb2a8b378dcf03aea57e86ced5fb18e2634b76c369c1a79c57bc6d63c2ad6e877363c6da5a996773b0716b8254a9c2dbbd9afe04  py3-unearth-0.11.1.tar.gz
"
